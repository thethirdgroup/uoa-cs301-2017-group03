// pseudocode for technical test 2:

//Bench mark test 2: Straight line
// Keep the line within the array

/*  Code to ensure that if the robot veers off to the left or right the psoc will adjust the motor speeds to turn / keep robot on the path
if (sensor1ADC < Value){
	Motor1AdjustLeft;
}

else if (sensor3ADC < Value){
	Motor1AdjustRight;
}
*/


// Bench mark test 3: Curve tracking
// Similar to Straight line test: Constantly check sensor ADC values to ensure that line is within the two sensors and adjust motor speed accordingly
/*
if (sensor1ADC < Value){
	Motor1AdjustLeft;
}

else if (sensor3ADC < Value){
	Motor1AdjustRight;
}
*/


// Bench mark test 4: Turn test
// Test 90 degree turns. ( could either continually poll sensor ADC values OR make a motorfunction that when executed instantly turns robot 90 degrees then continues. (counting wheel position etc.))
/*
if (if sensor1ADC && sensor2ADC < value){ // left 90 degree detected
	motorlefturn;
}
else if (sensor2ADC && sensor3ADC < value){ // Right 90 degree detected
	motorrightturnl;
}

else {
	motorstraight;
}


*/


//Bench mark test 5:  Speed + localisation test
// Using RF to transmit speed and distance, have robot run until certain distance met and at certain speed, straight.
/*
Distance = RFinput1;
Speed = RFinput2;

// convert speed into whatever
motorspeed(Speed);

while (distancetravelled < Distance){
	motorstraight;
}

*/


