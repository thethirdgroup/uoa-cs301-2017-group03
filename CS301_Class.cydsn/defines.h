/* ========================================
 *
 * Copyright Univ of Auckland, 2016
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
// Code setup
#define USE_USB
 
//* ========================================
#define FALSE 0
#define TRUE 1
#define UNKNOWN 10
//* ========================================
#define TS 100 // sample time = 100kHz * TS
#define DECIATE_TS_ENC     1000    // decimation factor for quad decoder
#define DECIMATE_TS_UPDATE  2000    // decimation factor for update action
#define DECIMATE_TS_SPEED   100
#define DECIMATE_TS_DISPLAY 1000
//* ========================================
// General Debug
#define LED_ON          LED_Write(1)
#define LED_OFF         LED_Write(0)
#define LED_TOGGLE      LED_Write(~LED_Read())
 
#define DB0_ON          DB0_Write(1)
#define DB0_OFF         DB0_Write(0)
#define DB0_TOGGLE_LED  DB0_Write(~DB0_Read())
 
#define DB1_ON          DB1_Write(1)
#define DB1_OFF         DB1_Write(0)
#define DB1_TOGGLE_LED  DB1_Write(~DB1_Read())
 
#define DB2_ON          DB2_Write(1)
#define DB2_OFF         DB2_Write(0)
#define DB2_TOGGLE_LED  DB2_Write(~DB2_Read())
//* ========================================
// Motor
#define COAST_OFF CONTROL_Write(0);
#define COAST_ON CONTROL_Write(0b11);
 
#define cmpulse 0.33
#define resolution 4
 
#define PWM_MAX 255     // maximum value of duty cycle
#define PWM_MIN 0       // minimum value of duty cycle
#define DIST_SCALE 0.9 // inertia comp
#define GRID_S 12.2
#define GRID_L 12.7
#define GRID 12.2 // DO NOT USE THIS JUST FOR TEST CASES
//* ========================================
// USBUART
#define BUF_SIZE 255 // USBUART fixed buffer size
#define CHAR_NULL '0'
#define CHAR_BACKSP 0x08
#define CHAR_DEL 0x7F
#define CHAR_ENTER 0x0D
#define LOW_DIGIT '0'
#define HIGH_DIGIT '9'
//* ========================================
//RF
#define SOP 0xaa
#define PACKETSIZE 32    
#define RXSTRINGSIZE 64 // 4+3+[4+4+4]+[4+4+4+4]+[4+4+4+4]+[4+4+4+4]+delimiters, i.e. no more than 64
#define GHOSTSIZE 3
//ADC
#define SENSOR1 5
#define SENSOR2 2
#define SENSOR3 7
#define SENSOR4 4
#define SENSOR5 3
#define SENSOR6 6
#define BATTERY 1
//Light
#define DARK_LIGHT 2.16 //2.16
#define DARK_LIGHT_BAD_CORNER 2.11
#define WHITE_LIGHT 2.61
 
//#define PWM_ADJUST 30
#define PWM_ADJUST 5
#define PWM_TURN 5
#define GRID_SCALE 0.85 // 85%
#define TURN_SPEED 66
#define TURN_DELAY 400

//Map
#define MAXCOL 19
#define MAXROW 15
#define MAXSIZE 19*15

/* [] END OF FILE */
 
#define S1_DARK (sensor1_voltage < darkThresh)
#define S1_LIGHT (sensor1_voltage > darkThresh)
#define S2_DARK (sensor2_voltage < darkThresh)
#define S2_LIGHT (sensor2_voltage > darkThresh)
#define S3_DARK (sensor3_voltage < darkThresh)
#define S3_LIGHT (sensor3_voltage > darkThresh)
#define S4_DARK (sensor4_voltage < darkThresh)
#define S4_LIGHT (sensor4_voltage > darkThresh)
#define S5_DARK (sensor5_voltage < darkThresh)
#define S5_LIGHT (sensor5_voltage > darkThresh)
#define S6_DARK (sensor6_voltage < darkThresh)
#define S6_LIGHT (sensor6_voltage > darkThresh)