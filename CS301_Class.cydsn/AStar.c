#include "AStar.h"
//----------------------------------------------------------------------------------------
int path[MAXSIZE][2];
int pathSize = 0;
//----------------------------------------------------------------------------------------
int neighbours[4][2] = {{0,0},
                        {0,0},
                        {0,0},
                        {0,0}};
int neighboursSize;
int openSetSize;

int parentRow[MAXROW][MAXCOL]; //Initialise all values to -1 using for loop
int parentCol[MAXROW][MAXCOL];
int F[MAXROW][MAXCOL];
int G[MAXROW][MAXCOL];
int visited[MAXROW][MAXCOL];
int closedSet[MAXROW][MAXCOL];
int rf_done;

int currentRow;
int currentCol;

void setRFDone() {
    rf_done = 1;
}

void clearRFDone() {
    rf_done = 0;
}

Node* createNode(int F, int G, int Row, int Col, Node* next) {
  Node* newNode = (Node*)malloc(sizeof(Node));
  if(newNode == NULL) {
    printf("Error creating new node.\n");
    exit(0);
  }
  newNode->F = F;
  newNode->G = G;
  newNode->Row = Row;
  newNode->Col = Col;
  newNode->next = next;
  return newNode;
}

void insertNode(int F, int G, int Row, int Col, Node* head) {
    Node* cursor = head;
    while(cursor->next != NULL) {
        cursor = cursor->next;
        //if the node to be added is already in the list, do not create/add it to the list and return
        //the current node.
        if ((cursor->Row == Row) && (cursor->Col == Col)) {
            return;
        }
    }

    Node* newNode = (Node*)malloc(sizeof(Node));
    if(newNode == NULL) {
        exit(0);
    }
    newNode->F = F;
    newNode->G = G;
    newNode->Row = Row;
    newNode->Col = Col;
    newNode->next = NULL;
    cursor->next = newNode;
    cursor = NULL;
}

Node* insertNodeToHead(int F, int G, int Row, int Col, Node* head) {
  Node* newNode = (Node*)malloc(sizeof(Node));
  if(newNode == NULL) {
    exit(0);
  }
  newNode->F = F;
  newNode->G = G;
  newNode->Row = Row;
  newNode->Col = Col;
  newNode->next = head;
  head = newNode;
  newNode = NULL;
  return head;
}

void deleteNode(Node* head, Node* node) {
  //If the node to delete is at front or middle copy next node and then free it.
  if (node->next != NULL) {
    Node* nextNode = node->next;
    node->F = nextNode->F;
    node->G = nextNode->G;
    node->Row = nextNode->Row;
    node->Col = nextNode->Col;
    node->next = nextNode->next;
    free(nextNode);
    nextNode= NULL;
  //} else if (head == node) {
    //free(head);
    //head = NULL;
    //node = NULL;
  }else {
    //If the node to delete is at the tail(end)
    //then go through linked list to get the second to last node and make its next point to NULL then delete the node to delete.
    Node* cursor = head;
    while(cursor->next != NULL) {
      if (cursor->next == node) {
        break;
      }
      cursor = cursor->next;
    }
    cursor->next = NULL;
    free(node);
    node = NULL;
  }
}

void deleteList(Node* head) {
  Node* cursor = head;
  while(head->next != NULL) {
    head = head->next;
    free(cursor);
    cursor = head;
  }
  free(head);
  head = NULL;
  cursor = NULL;
}

Node* getLowestF(Node* head) {
    Node* cursor = head;
    Node* minNode = head;
    int minF = -1;
    while(cursor->next != NULL) {
        if((minF == -1) || (cursor->F < minF)) {
            minF = cursor->F;
            minNode = cursor;
        }
        cursor = cursor->next;
    }
    cursor = NULL;

    return minNode;
}

int getListSize(Node* head) {
    Node* cursor = head;
    int listSize = 0;
    while(cursor->next != NULL) {
        listSize++;
        cursor = cursor->next;
    }
    listSize++;
    return listSize;
}

//Used to calculate heuristic for AStar
int manhattanDistance(int x1, int y1, int x2, int y2) {
  return abs(x1-x2) + abs(y1-y2);
}

void initialiseVars(int start[2], int target[2], int map[MAXROW][MAXCOL]) {
  int i = 0;
  int j = 0;
  for(i = 0; i < MAXROW; i++) {
    for(j = 0; j < MAXCOL; j++) {
      parentRow[i][j] = -1;
      parentCol[i][j] = -1;
      F[i][j] = -1;
      G[i][j] = -1;
      closedSet[i][j] = map[i][j];
    }
  }

  for(i = 0; i < MAXSIZE; i++) {
      for(j = 0; j < 2; j++) {
          path[i][j] = -1;
      }
  }

  currentRow = start[0];
  currentCol = start[1];
  G[currentRow][currentCol] = 0;
  F[currentRow][currentCol] = G[currentRow][currentCol] + manhattanDistance(currentRow,currentCol,target[0],target[1]);

}

void GetNeighbours(int visited[MAXROW][MAXCOL], int map[MAXROW][MAXCOL], int currentRow, int currentCol) {
  neighboursSize = 0;

  //Get left
  if((currentCol-1) >= 0) {
    if(map[currentRow][currentCol-1] == 0) {
      if(visited[currentRow][currentCol-1] == 0) {
        //Add to neighboursRow/neighboursCol
        neighbours[neighboursSize][0] = currentRow;
        neighbours[neighboursSize][1] = currentCol-1;
        neighboursSize++;
      }
    }
  }

  //Get right
  if((currentCol+1) < MAXCOL) {
    if(map[currentRow][currentCol+1] == 0) {
      if(visited[currentRow][currentCol+1] == 0) {
        neighbours[neighboursSize][0] = currentRow;
        neighbours[neighboursSize][1] = currentCol+1;
        neighboursSize++;
      }
    }
  }

  //Get up
  if((currentRow-1) >= 0) {
    if(map[currentRow-1][currentCol] == 0) {
      if(visited[currentRow-1][currentCol] == 0) {
        neighbours[neighboursSize][0] = currentRow-1;
        neighbours[neighboursSize][1] = currentCol;
        neighboursSize++;
      }
    }
  }

  //Get down
  if((currentRow+1) < MAXROW) {
    if(map[currentRow+1][currentCol] == 0) {
      if(visited[currentRow+1][currentCol] == 0) {
        neighbours[neighboursSize][0] = currentRow+1;
        neighbours[neighboursSize][1] = currentCol;
        neighboursSize++;
      }
    }
  }

}

void updateNeighboursCost(Node* head, int target[2]) {
  int tempG = G[currentRow][currentCol] + 1;
  int i = 0;
  for(i = 0; i < neighboursSize; i++) {
    int row = neighbours[i][0];
    int col = neighbours[i][1];
    if((G[row][col] == -1) || (tempG < G[row][col])) {
        G[row][col] = tempG;
        F[row][col] = G[row][col] + manhattanDistance(row,col,target[0],target[1]);
        parentRow[row][col] = currentRow;
        parentCol[row][col] = currentCol;
        insertNode(F[row][col],G[row][col],row,col,head);
        openSetSize++;
    }
  }
}

void retracePath(int start[2]) {
    if((parentRow[currentRow][currentCol] != -1) && (parentRow[currentRow][currentCol] != -1)) {
        int tempRow;
        int tempCol;
        pathSize = 0;
        /*
        Path starts from TARGET ends at START so to access the path must start from:
        path[pathSize-1] to path[0];
        */
        while (!((currentRow == start[0]) && (currentCol == start[1]))) {
            path[pathSize][0] = currentRow;
            path[pathSize][1] = currentCol;
            pathSize++;
            tempRow = parentRow[currentRow][currentCol];
            tempCol = parentCol[currentRow][currentCol];
            currentRow = tempRow;
            currentCol = tempCol;
        }
        //Must add the startRow/startCol to the path since while loop stops @ the start location.
        path[pathSize][0] = currentRow;
        path[pathSize][1] = currentCol;
        pathSize++;
    }
}

Node* AStar(int start[2], int target[2], int map[MAXROW][MAXCOL]) {
    initialiseVars(start,target,map);
    Node* openSetHead = createNode(F[currentRow][currentCol],G[currentRow][currentCol],currentRow,currentCol,NULL);
    Node* currentNode = openSetHead;
    openSetSize = 1;
    while(openSetSize != 0) {
        if((currentRow == target[0]) && (currentCol == target[1])) {
            break;
        }
        GetNeighbours(closedSet,map, currentRow, currentCol);
        updateNeighboursCost(openSetHead, target);
        closedSet[currentRow][currentCol] = 1;
        deleteNode(openSetHead, currentNode); //Delete the node with currentRow/currentCol
        openSetSize--;
        //Make currentNode point to openSetNode with lowest F cost
        currentNode = getLowestF(openSetHead);
        currentRow = currentNode->Row;
        currentCol = currentNode->Col;
    }
    retracePath(start);
    //Delete openSet as it is done
    if(openSetHead != NULL) {
      deleteList(openSetHead);
      openSetSize = 0;
    }

    int i = pathSize-1;
    Node* listPath = createNode(0,0,path[i][0],path[i][1],NULL);
    for(i = pathSize-2; i >= 0; i--) {
      insertNode(0,0,path[i][0],path[i][1],listPath);
    }
    return listPath;

}

void navigateMap(int start[2], int map[MAXROW][MAXCOL]) {
  Node* openSet = createNode(0,0,start[0],start[1],NULL);
  int newPos[2], currentPos[2];
  int nextPos[2];  
  uint8_t direction = getDirection(); //1 = NORTH, 2 = EAST, 3 = SOUTH, 4 = WEST
  currentPos[0] = getRobotY();
  currentPos[1] = getRobotX();
  int i,j;
  int visited[MAXROW][MAXCOL];

  for(i = 0; i < MAXROW; i++) {
    for(j = 0; j < MAXCOL; j++) {
      visited[i][j] = map[i][j];
    }
  }
    
  while(openSet != NULL) {
    direction = getDirection();  
    clearRFDone();
    while(rf_done == 0); //Wait for initial valid packet
    currentPos[0] = getRobotY();
    currentPos[1] = getRobotX();
    GetNeighbours(visited, map, currentPos[0], currentPos[1]);
    if (neighboursSize == 0) { // DEAD END
      deleteNode(openSet,openSet); //delete the branch that lead to dead end
      if(openSet != NULL) {
        nextPos[0] = openSet-> Row;
        nextPos[1] = openSet-> Col;
        //If we happened to visit one of the branches through another way remove it from openSet and go to the next openSet
        while (visited[nextPos[0]][nextPos[1]] == 1) {
          deleteNode(openSet,openSet);
          if(openSet != NULL) {
            nextPos[0] = openSet-> Row;
            nextPos[1] = openSet-> Col;
          } else {
            break; //Done;
          }
        }
      }

      if (openSet != NULL) {
        Node* BestPath = AStar(currentPos, nextPos, map);
        int pathSize = getListSize(BestPath);
        int pathArray[pathSize][2];
        Node* cursor = BestPath;
        for(j = 0; j < pathSize; j++) {
            pathArray[j][0] = cursor->Row;
            pathArray[j][1] = cursor->Col;
            visited[cursor->Row][cursor->Col] = 1;
            if(cursor->next != NULL) {
                cursor = cursor->next;
            }
        }
        deleteList(BestPath);
        free(cursor);
        cursor = NULL;
        AStarImplementation(pathArray,pathSize,currentPos); //Updates orientation automatically
      }
    } else if (neighboursSize > 1) { //INTERSECTION (Pick a path)
      //Remove first element in linkedList
      deleteNode(openSet,openSet);
      if (openSet == NULL) {
        openSet = createNode(0,0,neighbours[0][0],neighbours[0][1],NULL);
        for(i = 1; i < neighboursSize; i++) {
          openSet = insertNodeToHead(0,0,neighbours[i][0],neighbours[i][1],openSet);
        }
      } else { //If there is an existing openSet add to it
        for(i = 0; i < neighboursSize; i++) {
          openSet = insertNodeToHead(0, 0, neighbours[i][0], neighbours[i][1], openSet);
        }
      }
      //Don't need to update orientation as only travelled straight line
      //updateRFOrientation();
      //direction = getDirection();
      nextPos[0] = openSet->Row;
      nextPos[1] = openSet->Col;
      changeDirection(direction, currentPos, nextPos);
      UART_PutString("Straight\n");
      LineTracking(5,0); //Go to the next intersection
    } else if (neighboursSize == 1) { //If there is only 1 neighbour follow its position/line
      //updateRFOrientation();
      //direction = getDirection();
      nextPos[0] = neighbours[0][0]; //Neighbours Row
      nextPos[1] = neighbours[0][1]; //Neighbours Column
      changeDirection(direction, currentPos, nextPos);
      UART_PutString("Straight\n");
      LineTracking(5,0); // This will block until it reaches an intersection or dead end
    }
    //End picking/travelling path

    //If it wasn't a dead end then we will have travelled a line and must set the line travelled in visited to 1.
    if(neighboursSize != 0) { 
      clearRFDone();
      while(rf_done == 0); //Wait for new valid packet
      newPos[0] = getRobotY();
      newPos[1] = getRobotX();
      //After line tracking set all nodes before it as visited
      if (newPos[0] == currentPos[0]) { //NewRow same as OldRow
        if (newPos[1] > currentPos[1]) { //Moved EAST
          for(i = currentPos[1]; i <= newPos[1]; i++) { //From old column till new column
            visited[currentPos[0]][i] = 1;
          }
        } else if (newPos[1] < currentPos[1]) { //Moved WEST
          for(i = currentPos[1]; i >= newPos[1]; i--) { //From old column till new column
            visited[currentPos[0]][i] = 1;
          }
        }
      } else if (newPos[1] == currentPos[1]) { //NewCol same as OldCol 
        if (newPos[0] > currentPos[0]) { //Moved SOUTH
          for(i = currentPos[0]; i <= newPos[0]; i++) {
            visited[i][currentPos[1]] = 1;
          }
        } else if (newPos[0] < currentPos[0]) { //Moved NORTH
          for(i = currentPos[0]; i >= newPos[0]; i--) {
            visited[i][currentPos[1]] = 1;
          }
        }
      }
    }
    //End updating visited
  }

  //Double Check
  currentPos[0] = getRobotY();
  currentPos[1] = getRobotX();
  for(i = 0; i < MAXROW; i++) {
    for (j = 0; j < MAXCOL; j++) {
      if(visited[i][j] == 0) {
        nextPos[0] = i; 
        nextPos[1] = j;
        Node* BestPath = AStar(currentPos,nextPos,map);
        int pathSize = getListSize(BestPath);
        int pathArray[pathSize][2];
        Node* cursor = BestPath;
        int k;
        for(k = 0; k < pathSize; k++) {
            pathArray[k][0] = cursor->Row;
            pathArray[k][1] = cursor->Col;
            if(cursor->next != NULL) {
                cursor = cursor->next;
            }
        }
        deleteList(BestPath);
        free(cursor);
        cursor = NULL;
        AStarImplementation(pathArray,pathSize,currentPos);
        currentPos[0] = nextPos[0];
        currentPos[1] = nextPos[1];
      }
    }
  }
}

void changeDirection(uint8_t direction, int currentPos[2], int nextPos[2]) {
  if (direction == 1) { //Facing North
    if(nextPos[0] == currentPos[0]) { //Next Position in same row
      if(nextPos[1] == currentPos[1]+1) { //Going EAST
        //turnRight
        UART_PutString("Turn Right\n");
        AdjustLine(2);
        AdjustLine(0);
        updateOrientation(2);
      } else if (nextPos[1] == currentPos[1]-1) { //Going WEST
        //turnLeft
        UART_PutString("Turn Left\n");
        AdjustLine(1);
        AdjustLine(0);
        updateOrientation(1);
      }
    } else if (nextPos[1] == currentPos[1]) { //Next position in same column
      if(nextPos[0] == currentPos[0]+1) { //Going SOUTH
        UART_PutString("Turn Around\n");
        TurnAround();
        setOrientation(3); //FACE SOUTH
      } else if (nextPos[0] == currentPos[0]-1) { //Going NORTH
        UART_PutString("Continue Staright\n");
        //continueStraight
      }
    }
  } else if (direction == 2) { //Facing EAST
    if (nextPos[0] == currentPos[0]) { //Next position in same row
      if(nextPos[1] == currentPos[1]+1) { //Going EAST
        //continueStraight
        UART_PutString("Continue Straight\n");
      } else if (nextPos[1] == currentPos[1]-1) {//Going WEST
        TurnAround();
        setOrientation(4); //FACE WEST
        UART_PutString("Turn Around\n");
        
      }
    } else if (nextPos[1] == currentPos[1]) { //Next position in same column
      if(nextPos[0] == currentPos[0]+1) { //Going SOUTH
        //turnRight
        UART_PutString("Turn Right\n");
        AdjustLine(2);
        AdjustLine(0);
        updateOrientation(2);
      } else if (nextPos[0] == currentPos[0]-1) { //Going NORTH
        //turnLeft
        UART_PutString("Turn Left\n");
        AdjustLine(1);
        AdjustLine(0);
        updateOrientation(1);
      }
    }
  } else if (direction == 3) { //Facing SOUTH
    if(nextPos[0] == currentPos[0]) { //Next position in same row
      if(nextPos[1] == currentPos[1]+1) { //Going EAST
        //turnLeft
        UART_PutString("Turn Left\n");
        AdjustLine(1);
        AdjustLine(0);
        updateOrientation(1);
      } else if (nextPos[1] == currentPos[1]-1) { //Going WEST
        //turnRight
        UART_PutString("Turn Right\n");
        AdjustLine(2);
        AdjustLine(0);
        updateOrientation(2);
      }
    } else if (nextPos[1] == currentPos[1]) { //Next position in same column
      if(nextPos[0] == currentPos[0]+1) { //Going SOUTH
        //continueStraight
        UART_PutString("Continue Straight\n");
      } else if (nextPos[0] == currentPos[0]-1) { //Going NORTH
        UART_PutString("Turn Around\n");
        TurnAround();
        setOrientation(1);
      }
    }
  } else if (direction == 4) { //Facing WEST
    if(nextPos[0] == currentPos[0]) { //Next position in same row
      if(nextPos[1] == currentPos[1]+1) { //Going EAST
        UART_PutString("Turn Around\n");
        TurnAround();
        setOrientation(2);
      } else if (nextPos[1] == currentPos[1]-1) { //Going WEST
        //continueStraight
        UART_PutString("Continue Straight\n");
      }
    } else if (nextPos[1] == currentPos[1]) { //Next position in same column
      if(nextPos[0] == currentPos[0]+1) { //Going SOUTH
        //turnLeft
        UART_PutString("Turn Left\n");
        AdjustLine(1);
        AdjustLine(0);
        updateOrientation(1);
      } else if (nextPos[0] == currentPos[0]-1) { //Going NORTH
        //turnRight
        UART_PutString("Turn Right\n");
        AdjustLine(2);
        AdjustLine(0);
        updateOrientation(2);
      }
    }
  }
}
