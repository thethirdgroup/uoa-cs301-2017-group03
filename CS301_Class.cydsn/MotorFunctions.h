/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef MOTORFUNCTIONS_H_INCLUDED
#define MOTORFUNCTIONS_H_INCLUDED

#include <project.h>
#include "RobotMovement.h"
    
void MotorControlINIT();
void Motor1INIT();
void Motor2INIT();
void Motor1Slow();
void Motor2Slow();
void Motor1Fast();
void Motor1Stop();
void Motor2Stop();
void Motor1Start();
void Motor2Start();
void Motor2Fast();
void Motor1Reverse();
void Motor2Reverse();
void Motor1Forward();
void Motor2Forward();
void MotorLeftTurn();
void MotorRightTurn();
void MotorSlowLeftTurn();
void MotorSlowRightTurn();
void MotorStop();
void Motor1Speedup();
void Motor2Speedup();
void Motor1Slowdown();
void Motor2Slowdown();
void AdjustLeft();
void AdjustRight();
void PivotLeft();
void PivotRight();
void TurnLeft();
void TurnRight();
void TurnAround();
void GoStraight();
void GoBack();
void Stop();



#endif

/* [] END OF FILE */
