#include "RF.h"

//RF DATA
int8_t rssi = 0;
uint8_t indx = 0;
uint16_t robot_x = 0;
uint16_t robot_y = 0;
uint16_t robot_orientation = 0;
uint16_t ghost_x[GHOSTSIZE] = {0};
uint16_t ghost_y[GHOSTSIZE] = {0};
uint16_t ghost_pps[GHOSTSIZE] = {0};
uint8_t ghost_dir[GHOSTSIZE] = {0};

int8_t getRSSI() {
    return rssi;
}

uint8_t getIndex() {
    return indx;
}

uint16_t getRobotX() {
    return robot_x;
}

uint16_t getRobotY() {
    return robot_y;
}

uint16_t getRobotOrientation() {
    return robot_orientation;
}

uint16_t getGhostX(int ghostNumber) {
    return ghost_x[ghostNumber];
}

uint16_t getGhostY(int ghostNumber) {
    return ghost_y[ghostNumber];
}

uint16_t getGhostPPS(int ghostNumber) {
    return ghost_pps[ghostNumber];
}

uint8_t getGhostDir(int ghostNumber) {
    return ghost_dir[ghostNumber];
}

void parseString(volatile char rxString[]) {

    char* temp;
    
    temp = strtok((char*) rxString, "#,[]");
    rssi = (int8_t) strtol(temp,NULL,10);
    
    if (rssi >= 0) {
        //Bad Packet
        return;
    }

    temp = strtok(NULL,"#,[]");
    indx = (uint8_t) strtol(temp,NULL,10);

    temp = strtok(NULL,"#,[]");
    robot_x = (uint16_t) strtol(temp,NULL,10)/53; //1024/19 = 53.

    temp = strtok(NULL,"#,[]");
    robot_y = (uint16_t) strtol(temp,NULL,10)/51; //768/15 = 51 * pixel_size?

    temp = strtok(NULL,"#,[]");
    robot_orientation = (uint16_t) strtol(temp,NULL,10); //Angle is multiplied by 10

    temp = strtok(NULL,"#,[]");
    ghost_x[0] = (uint16_t) strtol(temp,NULL,10);

    temp = strtok(NULL,"#,[]");
    ghost_y[0] = (uint16_t) strtol(temp,NULL,10);

    temp = strtok(NULL,"#,[]");
    ghost_pps[0] = (uint16_t) strtol(temp,NULL,10);

    temp = strtok(NULL,"#,[]");
    ghost_dir[0] = (uint16_t) strtol(temp,NULL,10);
    
    temp = strtok(NULL,"#,[]");
    ghost_x[1] = (uint16_t) strtol(temp,NULL,10);

    temp = strtok(NULL,"#,[]");
    ghost_y[1] = (uint16_t) strtol(temp,NULL,10);

    temp = strtok(NULL,"#,[]");
    ghost_pps[1] = (uint16_t) strtol(temp,NULL,10);

    temp = strtok(NULL,"#,[]");
    ghost_dir[1] = (uint16_t) strtol(temp,NULL,10);

    temp = strtok(NULL,"#,[]");
    ghost_x[2] = (uint16_t) strtol(temp,NULL,10);
    
    temp = strtok(NULL,"#,[]");
    ghost_y[2] = (uint16_t) strtol(temp,NULL,10);
    
    temp = strtok(NULL,"#,[]");
    ghost_pps[2] = (uint16_t) strtol(temp,NULL,10);
    
    temp = strtok(NULL,"#,[]");
    ghost_dir[2] = (uint16_t) strtol(temp,NULL,10);
       
}

void BT_WriteString(char* string){
    UART_PutString(string); 
 }
