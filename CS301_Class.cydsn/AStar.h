#include <stdio.h>
#include <stdlib.h>
#include "RobotMovement.h"
#include "defines.h"
//-----------------------------------------------------------------------------------------

//Linked List structure
typedef struct Node {
  int F,G,Row,Col;
  struct Node* next;
} Node;

void setRFDone();
void clearRFDone();
//Linked List functions
Node* createNode(int F, int G, int Row, int Col, Node* next);
void insertNode(int F, int G, int Row, int Col, Node* head);
Node* insertNodeToHead(int F, int G, int Row, int Col, Node* head);
void deleteNode(Node* head, Node* node);
void deleteList(Node* head);
Node* getLowestF(Node* head);
int getListSize(Node* head);
//AStar Functions
int manhattanDistance(int x1, int y1, int x2, int y2);
void initialiseVars(int start[2], int target[2], int map[MAXROW][MAXCOL]);
void GetNeighbours(int visited[MAXROW][MAXCOL], int map[MAXROW][MAXCOL], int currentRow, int currentCol);
void updateNeighboursCost(Node* head, int target[2]);
void retracePath(int start[2]);
Node* AStar(int start[2], int target[2], int map[MAXROW][MAXCOL]);
//Mode 1
void navigateMap(int start[2], int map[MAXROW][MAXCOL]);  
void changeDirection(uint8_t direction, int currentPos[2], int nextPos[2]);

/* [] END OF FILE */
