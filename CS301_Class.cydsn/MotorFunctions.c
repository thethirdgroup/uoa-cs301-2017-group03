/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <project.h>
#include "defines.h"
//* ========================================
#include "MotorFunctions.h"
//* ========================================

int local_count = 0;
int local_M2QuadCount;
int local_M1QuadCount;
int local_left_limit = 60;
int local_right_limit = 60;

/*******************************************************************************
* Function Name: MotorControlINIT
Starts all required variables for both motors (control registers etc)
******************************************************************************/
void MotorControlINIT()
{
    CONTROL_Write(0b00);
}

/*******************************************************************************
* Function Name: Motor1INIT
Starts all required variables for motor 1
******************************************************************************/
void Motor1INIT() // remember, 0 is fastest
{
    PWM_1_WritePeriod(255);    // Initialize PWM 1
    PWM_1_Start();
    PWM_1_WriteCompare(127);
    
    M1_EN_Write(1);  // Enable Motor 1
    M1_INV_Write(1);  // Enable Inversion (FORWARD)
}

/*******************************************************************************
* Function Name: Motor2INIT
Starts all required variables for motor 2
******************************************************************************/
void Motor2INIT()
{
    PWM_2_WritePeriod(255);    // Initialize PWM 1
    PWM_2_Start();
    PWM_2_WriteCompare(127);
    //PWM_2_WriteCompare(25);
    
    M2_EN_Write(1);  // Enable Motor 1
    M2_INV_Write(0);  // Enable Inversion (FORWARD)
}

// NOTE: All above functions must be called before process is operational

/*******************************************************************************
* Function Name: Motor1Slow
Sets motor 1 speed to slow
******************************************************************************/
void Motor1Slow()
{  
    PWM_1_WriteCompare(40);
}

/*******************************************************************************
* Function Name: Motor2Slow
Sets motor 2 speed to slow
******************************************************************************/
void Motor2Slow()
{  
    PWM_2_WriteCompare(40);
}

/*******************************************************************************
* Function Name: Motor1Fast
Sets motor 1 speed to fast
******************************************************************************/
void Motor1Fast()
{  
    PWM_1_WriteCompare(0);
}

/*******************************************************************************
* Function Name: Motor2Fast
Sets motor 2 speed to fast
******************************************************************************/
void Motor2Fast()
{  
    PWM_2_WriteCompare(0);
}

/*******************************************************************************
* Function Name: Motor1Stop
Make Motor1 go in reverse
******************************************************************************/
void Motor1Stop()
{  
    M1_EN_Write(0);
}

void Motor1Start()
{  
    M1_EN_Write(1);
}
/*******************************************************************************
* Function Name: Motor2Stop
Make Motor2 go in reverse
******************************************************************************/
void Motor2Stop()
{  
    M2_EN_Write(0);
}

void Motor2Start()
{  
    M2_EN_Write(1);
}

/*******************************************************************************
* Function Name: Motor1Reverse
Make Motor1 go in reverse
******************************************************************************/
void Motor1Reverse()
{  
    M1_INV_Write(0);
}

/*******************************************************************************
* Function Name: Motor2Reverse
Make Motor2 go in reverse
******************************************************************************/
void Motor2Reverse()
{  
    M2_INV_Write(1);
}

/*******************************************************************************
* Function Name: Motor1Forward
Make Motor1 go in forward
******************************************************************************/
void Motor1Forward()
{  
    M1_INV_Write(1);
}

/*******************************************************************************
* Function Name: Motor2Forward
Make Motor2 go in forward
******************************************************************************/
void Motor2Forward()
{  
    M2_INV_Write(0);
}

/*******************************************************************************
* Function Name: MotorLeftTurn
Makes a left turn
******************************************************************************/
void MotorLeftTurn()
{  
    PWM_1_WriteCompare(0);
    PWM_2_WriteCompare(30);
}

/*******************************************************************************
* Function Name: MotorRightTurn
Makes a Right Turn
******************************************************************************/
void MotorRightTurn()
{  
   PWM_1_WriteCompare(30);
   PWM_2_WriteCompare(0);
}

/*******************************************************************************
* Function Name: MotorSlowLeftTurn
Makes a left turn
******************************************************************************/
void MotorSlowLeftTurn()
{  
    PWM_1_WriteCompare(20);
    PWM_2_WriteCompare(40);
}

/*******************************************************************************
* Function Name: MotorSlowRightTurn
Makes a Right Turn
******************************************************************************/
void MotorSlowRightTurn()
{  
   PWM_1_WriteCompare(40);
   PWM_2_WriteCompare(20);
}

/*******************************************************************************
* Function Name: MotorStraight
Goes Straight
******************************************************************************/
void MotorStraight()
{  
   PWM_1_WriteCompare(30);
   PWM_2_WriteCompare(30);
}

/*******************************************************************************
* Function Name: MotorStop
Stops the Motor
******************************************************************************/
void MotorDisable()
{  
   M1_EN_Write(0);
   M2_EN_Write(0);
}

void AdjustLeft(int Limit)
{

    PWM_1_WriteCompare(Limit + 1);
    PWM_2_WriteCompare(Limit - 1);
    M1_INV_Write(0);
    M2_INV_Write(0);
}

void AdjustRight(int Limit){

    PWM_1_WriteCompare(Limit - 1);
    PWM_2_WriteCompare(Limit + 1);
    M1_INV_Write(1);
    M2_INV_Write(1);
}

void TurnLeft() {
    int M2_Flag = 0;
    QuadDec_M2_SetCounter(0);
    QuadDec_M1_SetCounter(0);
    while (!M2_Flag) {
        if (QuadDec_M2_GetCounter() < 60){
            PWM_1_WriteCompare(60);    
            PWM_2_WriteCompare(60);  
            M1_INV_Write(0);
            M2_INV_Write(0);
        } else {
            PWM_1_WriteCompare(127); // stop
            PWM_2_WriteCompare(127); // stop
            M2_Flag = 1;
        }
    }
    QuadDec_M2_SetCounter(0);
    QuadDec_M1_SetCounter(0);
}

void TurnRight(){
    int M2_Flag = 0;
    while (!M2_Flag) {
        if (QuadDec_M2_GetCounter() > -90){
            PWM_1_WriteCompare(60);    
            PWM_2_WriteCompare(60);  
            M1_INV_Write(1);
            M2_INV_Write(1);
        } else {
            PWM_1_WriteCompare(127); // stop
            PWM_2_WriteCompare(127); // stop
            M2_Flag = 1;
        }
    }
    QuadDec_M2_SetCounter(0);
    QuadDec_M1_SetCounter(0);
}

void TurnAround() {
    TurnLeft();
    CyDelay(5); // small delay
    TurnLeft();
    CyDelay(5);
    AdjustLine(1);
    CyDelay(5);
    AdjustLine(0);
    CyDelay(5);
    
}

void PivotRight() {
    PWM_1_WriteCompare(TURN_SPEED);    
    PWM_2_WriteCompare(TURN_SPEED);  
    M1_INV_Write(1);
    M2_INV_Write(1);
}

void PivotLeft() {
    PWM_1_WriteCompare(TURN_SPEED);    
    PWM_2_WriteCompare(TURN_SPEED);  
    M1_INV_Write(0);
    M2_INV_Write(0);
}


void GoStraight(int limit){   
    int M2QuadCount = QuadDec_M2_GetCounter(); // current value of counter
    int M1QuadCount = QuadDec_M1_GetCounter(); // current value of counter
    float motor1dist = ((M1QuadCount/resolution)*cmpulse);
    float motor2dist = ((M2QuadCount/resolution)*cmpulse);
    
    PWM_1_WriteCompare(limit);
    PWM_2_WriteCompare(limit); 
    M1_INV_Write(1);
    M2_INV_Write(0);
    
    if (motor1dist > motor2dist){ // motor 1 has gone further than motor2 (motor1dist > motor2dist)
        Motor1Slowdown(limit+2);
        Motor2Speedup(limit);
    } else if (motor2dist > motor1dist){ // motor 2 has gone farther than motor 1 (motor2dist > motor1dist)
        Motor1Speedup(limit);
        Motor2Slowdown(limit+2);
    } else {
        Motor1Speedup(limit);
        Motor2Slowdown(limit);
    }
}

void Motor1Speedup(int Writecomp){
		Writecomp = Writecomp - 1;
		PWM_1_WriteCompare(Writecomp);
}

void Motor2Speedup(int Writecomp){
		Writecomp = Writecomp - 1;
		PWM_2_WriteCompare(Writecomp);
}

void Motor1Slowdown(int Writecomp){
		Writecomp = Writecomp + 1;
		PWM_1_WriteCompare(Writecomp);
}

void Motor2Slowdown(int Writecomp){
		Writecomp = Writecomp + 1;
		PWM_2_WriteCompare(Writecomp);
}

void Stop(){
     PWM_1_WriteCompare(127); // stop motor
     PWM_2_WriteCompare(127);   
}

/* [] END OF FILE */