/*******************************************************************************
* File Name: ModeSelect1.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_ModeSelect1_H) /* Pins ModeSelect1_H */
#define CY_PINS_ModeSelect1_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "ModeSelect1_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 ModeSelect1__PORT == 15 && ((ModeSelect1__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    ModeSelect1_Write(uint8 value);
void    ModeSelect1_SetDriveMode(uint8 mode);
uint8   ModeSelect1_ReadDataReg(void);
uint8   ModeSelect1_Read(void);
void    ModeSelect1_SetInterruptMode(uint16 position, uint16 mode);
uint8   ModeSelect1_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the ModeSelect1_SetDriveMode() function.
     *  @{
     */
        #define ModeSelect1_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define ModeSelect1_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define ModeSelect1_DM_RES_UP          PIN_DM_RES_UP
        #define ModeSelect1_DM_RES_DWN         PIN_DM_RES_DWN
        #define ModeSelect1_DM_OD_LO           PIN_DM_OD_LO
        #define ModeSelect1_DM_OD_HI           PIN_DM_OD_HI
        #define ModeSelect1_DM_STRONG          PIN_DM_STRONG
        #define ModeSelect1_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define ModeSelect1_MASK               ModeSelect1__MASK
#define ModeSelect1_SHIFT              ModeSelect1__SHIFT
#define ModeSelect1_WIDTH              1u

/* Interrupt constants */
#if defined(ModeSelect1__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in ModeSelect1_SetInterruptMode() function.
     *  @{
     */
        #define ModeSelect1_INTR_NONE      (uint16)(0x0000u)
        #define ModeSelect1_INTR_RISING    (uint16)(0x0001u)
        #define ModeSelect1_INTR_FALLING   (uint16)(0x0002u)
        #define ModeSelect1_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define ModeSelect1_INTR_MASK      (0x01u) 
#endif /* (ModeSelect1__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define ModeSelect1_PS                     (* (reg8 *) ModeSelect1__PS)
/* Data Register */
#define ModeSelect1_DR                     (* (reg8 *) ModeSelect1__DR)
/* Port Number */
#define ModeSelect1_PRT_NUM                (* (reg8 *) ModeSelect1__PRT) 
/* Connect to Analog Globals */                                                  
#define ModeSelect1_AG                     (* (reg8 *) ModeSelect1__AG)                       
/* Analog MUX bux enable */
#define ModeSelect1_AMUX                   (* (reg8 *) ModeSelect1__AMUX) 
/* Bidirectional Enable */                                                        
#define ModeSelect1_BIE                    (* (reg8 *) ModeSelect1__BIE)
/* Bit-mask for Aliased Register Access */
#define ModeSelect1_BIT_MASK               (* (reg8 *) ModeSelect1__BIT_MASK)
/* Bypass Enable */
#define ModeSelect1_BYP                    (* (reg8 *) ModeSelect1__BYP)
/* Port wide control signals */                                                   
#define ModeSelect1_CTL                    (* (reg8 *) ModeSelect1__CTL)
/* Drive Modes */
#define ModeSelect1_DM0                    (* (reg8 *) ModeSelect1__DM0) 
#define ModeSelect1_DM1                    (* (reg8 *) ModeSelect1__DM1)
#define ModeSelect1_DM2                    (* (reg8 *) ModeSelect1__DM2) 
/* Input Buffer Disable Override */
#define ModeSelect1_INP_DIS                (* (reg8 *) ModeSelect1__INP_DIS)
/* LCD Common or Segment Drive */
#define ModeSelect1_LCD_COM_SEG            (* (reg8 *) ModeSelect1__LCD_COM_SEG)
/* Enable Segment LCD */
#define ModeSelect1_LCD_EN                 (* (reg8 *) ModeSelect1__LCD_EN)
/* Slew Rate Control */
#define ModeSelect1_SLW                    (* (reg8 *) ModeSelect1__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define ModeSelect1_PRTDSI__CAPS_SEL       (* (reg8 *) ModeSelect1__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define ModeSelect1_PRTDSI__DBL_SYNC_IN    (* (reg8 *) ModeSelect1__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define ModeSelect1_PRTDSI__OE_SEL0        (* (reg8 *) ModeSelect1__PRTDSI__OE_SEL0) 
#define ModeSelect1_PRTDSI__OE_SEL1        (* (reg8 *) ModeSelect1__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define ModeSelect1_PRTDSI__OUT_SEL0       (* (reg8 *) ModeSelect1__PRTDSI__OUT_SEL0) 
#define ModeSelect1_PRTDSI__OUT_SEL1       (* (reg8 *) ModeSelect1__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define ModeSelect1_PRTDSI__SYNC_OUT       (* (reg8 *) ModeSelect1__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(ModeSelect1__SIO_CFG)
    #define ModeSelect1_SIO_HYST_EN        (* (reg8 *) ModeSelect1__SIO_HYST_EN)
    #define ModeSelect1_SIO_REG_HIFREQ     (* (reg8 *) ModeSelect1__SIO_REG_HIFREQ)
    #define ModeSelect1_SIO_CFG            (* (reg8 *) ModeSelect1__SIO_CFG)
    #define ModeSelect1_SIO_DIFF           (* (reg8 *) ModeSelect1__SIO_DIFF)
#endif /* (ModeSelect1__SIO_CFG) */

/* Interrupt Registers */
#if defined(ModeSelect1__INTSTAT)
    #define ModeSelect1_INTSTAT            (* (reg8 *) ModeSelect1__INTSTAT)
    #define ModeSelect1_SNAP               (* (reg8 *) ModeSelect1__SNAP)
    
	#define ModeSelect1_0_INTTYPE_REG 		(* (reg8 *) ModeSelect1__0__INTTYPE)
#endif /* (ModeSelect1__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_ModeSelect1_H */


/* [] END OF FILE */
