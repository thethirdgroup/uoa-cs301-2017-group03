/*******************************************************************************
* File Name: ModeSelect2.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_ModeSelect2_H) /* Pins ModeSelect2_H */
#define CY_PINS_ModeSelect2_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "ModeSelect2_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 ModeSelect2__PORT == 15 && ((ModeSelect2__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    ModeSelect2_Write(uint8 value);
void    ModeSelect2_SetDriveMode(uint8 mode);
uint8   ModeSelect2_ReadDataReg(void);
uint8   ModeSelect2_Read(void);
void    ModeSelect2_SetInterruptMode(uint16 position, uint16 mode);
uint8   ModeSelect2_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the ModeSelect2_SetDriveMode() function.
     *  @{
     */
        #define ModeSelect2_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define ModeSelect2_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define ModeSelect2_DM_RES_UP          PIN_DM_RES_UP
        #define ModeSelect2_DM_RES_DWN         PIN_DM_RES_DWN
        #define ModeSelect2_DM_OD_LO           PIN_DM_OD_LO
        #define ModeSelect2_DM_OD_HI           PIN_DM_OD_HI
        #define ModeSelect2_DM_STRONG          PIN_DM_STRONG
        #define ModeSelect2_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define ModeSelect2_MASK               ModeSelect2__MASK
#define ModeSelect2_SHIFT              ModeSelect2__SHIFT
#define ModeSelect2_WIDTH              1u

/* Interrupt constants */
#if defined(ModeSelect2__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in ModeSelect2_SetInterruptMode() function.
     *  @{
     */
        #define ModeSelect2_INTR_NONE      (uint16)(0x0000u)
        #define ModeSelect2_INTR_RISING    (uint16)(0x0001u)
        #define ModeSelect2_INTR_FALLING   (uint16)(0x0002u)
        #define ModeSelect2_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define ModeSelect2_INTR_MASK      (0x01u) 
#endif /* (ModeSelect2__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define ModeSelect2_PS                     (* (reg8 *) ModeSelect2__PS)
/* Data Register */
#define ModeSelect2_DR                     (* (reg8 *) ModeSelect2__DR)
/* Port Number */
#define ModeSelect2_PRT_NUM                (* (reg8 *) ModeSelect2__PRT) 
/* Connect to Analog Globals */                                                  
#define ModeSelect2_AG                     (* (reg8 *) ModeSelect2__AG)                       
/* Analog MUX bux enable */
#define ModeSelect2_AMUX                   (* (reg8 *) ModeSelect2__AMUX) 
/* Bidirectional Enable */                                                        
#define ModeSelect2_BIE                    (* (reg8 *) ModeSelect2__BIE)
/* Bit-mask for Aliased Register Access */
#define ModeSelect2_BIT_MASK               (* (reg8 *) ModeSelect2__BIT_MASK)
/* Bypass Enable */
#define ModeSelect2_BYP                    (* (reg8 *) ModeSelect2__BYP)
/* Port wide control signals */                                                   
#define ModeSelect2_CTL                    (* (reg8 *) ModeSelect2__CTL)
/* Drive Modes */
#define ModeSelect2_DM0                    (* (reg8 *) ModeSelect2__DM0) 
#define ModeSelect2_DM1                    (* (reg8 *) ModeSelect2__DM1)
#define ModeSelect2_DM2                    (* (reg8 *) ModeSelect2__DM2) 
/* Input Buffer Disable Override */
#define ModeSelect2_INP_DIS                (* (reg8 *) ModeSelect2__INP_DIS)
/* LCD Common or Segment Drive */
#define ModeSelect2_LCD_COM_SEG            (* (reg8 *) ModeSelect2__LCD_COM_SEG)
/* Enable Segment LCD */
#define ModeSelect2_LCD_EN                 (* (reg8 *) ModeSelect2__LCD_EN)
/* Slew Rate Control */
#define ModeSelect2_SLW                    (* (reg8 *) ModeSelect2__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define ModeSelect2_PRTDSI__CAPS_SEL       (* (reg8 *) ModeSelect2__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define ModeSelect2_PRTDSI__DBL_SYNC_IN    (* (reg8 *) ModeSelect2__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define ModeSelect2_PRTDSI__OE_SEL0        (* (reg8 *) ModeSelect2__PRTDSI__OE_SEL0) 
#define ModeSelect2_PRTDSI__OE_SEL1        (* (reg8 *) ModeSelect2__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define ModeSelect2_PRTDSI__OUT_SEL0       (* (reg8 *) ModeSelect2__PRTDSI__OUT_SEL0) 
#define ModeSelect2_PRTDSI__OUT_SEL1       (* (reg8 *) ModeSelect2__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define ModeSelect2_PRTDSI__SYNC_OUT       (* (reg8 *) ModeSelect2__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(ModeSelect2__SIO_CFG)
    #define ModeSelect2_SIO_HYST_EN        (* (reg8 *) ModeSelect2__SIO_HYST_EN)
    #define ModeSelect2_SIO_REG_HIFREQ     (* (reg8 *) ModeSelect2__SIO_REG_HIFREQ)
    #define ModeSelect2_SIO_CFG            (* (reg8 *) ModeSelect2__SIO_CFG)
    #define ModeSelect2_SIO_DIFF           (* (reg8 *) ModeSelect2__SIO_DIFF)
#endif /* (ModeSelect2__SIO_CFG) */

/* Interrupt Registers */
#if defined(ModeSelect2__INTSTAT)
    #define ModeSelect2_INTSTAT            (* (reg8 *) ModeSelect2__INTSTAT)
    #define ModeSelect2_SNAP               (* (reg8 *) ModeSelect2__SNAP)
    
	#define ModeSelect2_0_INTTYPE_REG 		(* (reg8 *) ModeSelect2__0__INTTYPE)
#endif /* (ModeSelect2__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_ModeSelect2_H */


/* [] END OF FILE */
