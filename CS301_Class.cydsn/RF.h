#include <stdio.h>
#include <stdlib.h>
#include <project.h>
#include "defines.h"

int8_t getRSSI();
uint8_t getIndex();
uint16_t getRobotX();
uint16_t getRobotY();
uint16_t getRobotOrientation();
uint16_t getGhostX(int ghostNumber);
uint16_t getGhostY(int ghostNumber);
uint16_t getGhostPPS(int ghostNumber);
uint8_t getGhostDir(int ghostNumber);                                    
void parseString(volatile char rxString[]);
void BT_WriteString(char* string);
