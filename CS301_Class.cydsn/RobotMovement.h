#include <stdio.h>
#include <stdlib.h>
#include <project.h>
#include "defines.h"
#include "RF.h"
#include "MotorFunctions.h"

void setMapData(int map[MAXROW][MAXCOL]);
void setMode(int newMode);
void TestMovement(char* completeString, uint8_t rf_done);
void readADC(void); // read all needed ADC channels
void UpdateTurnData(void);
void AdjustLine(int TurnData);
void LineTracking(float distance, int food);
int DeadEnd(void);
void AStarImplementation(int pathArray[][2], int lengthOfArray, int start[2]);
void updateRFOrientation();// get orientation of robot: +- 30 degrees
void updateOrientation(int turnDir);// called everytime we turnLeft (= 1) or turnRight (= 2)
void L(void);
void R(void);
void S(double distance);
void path1(void);
void path2(void);
uint8_t getDirection(void);
void BT_WriteADC();
void setOrientation(int a);
void updateThreshold(void);
void RFAlign(int turnData);

