#include "RobotMovement.h"

int limit = 62;//63
unsigned int EOC = 0;
int start = 0;
int turn = 0; // 1 left, 2 right
float averagelight = 0;
int no_whitelight = 0;
int mode = 0;

//PWM and Motor
int count = 0;
int PWM1duty = 0; // Read counter value for PWM1 (M1)
int PWM2duty = 0;	// Read duty cycle of PWM2 (M2)
int deadEnd = 0;
uint16_t M2QuadCount = 0; // current value of counter
uint16_t M1QuadCount = 0; // current value of counter
uint16_t motor1dist = 0;
uint16_t motor2dist = 0;
float darkThresh = DARK_LIGHT;


//A*
uint8_t orientation;

//ADC
float battery_voltage = 9.999; // battery 
float sensor1_voltage = 9.999;
float sensor2_voltage = 9.999;
float sensor3_voltage = 9.999;
float sensor4_voltage = 9.999;
float sensor5_voltage = 9.999;
float sensor6_voltage = 9.999;

//Map
int mapA[MAXROW][MAXCOL];

void setMapData(int map[MAXROW][MAXCOL]) {
    int i,j;
    for(i = 0; i < MAXROW; i++) {
        for(j = 0; j < MAXCOL; j++) {
            mapA[i][j] = map[i][j];
        }
    }
}

void setMode(int newMode) {
    mode = newMode;
}    

void TestMovement(char* completeString, uint8_t rf_done)
{
    LED_OFF;
    RF_BT_SELECT_Write(1); // bluetooth enable
    //completeString = '';
    while (rf_done == 0)
    { // have not receive antthing block
    }
    //RF_BT_SELECT_Write(0); // rf enable
    /*parseString(completeString);
        if (USB_DEBUG) {
            printRX();
            UART_PutChar('\n');
        }
        */
    UART_PutString("Fuck off eugene\n");
    UART_PutString(completeString);
    UART_PutString("\n");
 
    rf_done = 0;
 
    /*if (flag_KB_string == 1) {
        (line);
        flag_KB_string = 0;
    }*/
 
    if (strstr(completeString, "S0") != NULL)
    {
        LED_ON;
        LineTracking(5, 0);
    }
    else if (strstr(completeString, "S1") != NULL)
    {
        LED_ON;
        LineTracking(11, 1);
    }
    else if (strstr(completeString, "S2") != NULL)
    {
        LED_ON;
        LineTracking(22, 1);
    }
    else if (strstr(completeString, "L") != NULL)
    {
        LED_ON;
        AdjustLine(1); // turn left
        AdjustLine(0); // adjust until its on line
    }
    else if (strstr(completeString, "R") != NULL)
    {
        LED_ON;
        AdjustLine(2); // turn right
        AdjustLine(0); // adjust until its on line
    }
    else if (strstr(completeString, "start") != NULL)
    {
        LED_ON;
        start = 1;
    }
}

/*
Read all ADC channels when conversion is complete.
*/
void readADC(void) {
    ADC_StartConvert(); // change this because its blocking 
    EOC = 0;
    while(EOC == 0){
        EOC = ADC_IsEndConversion(ADC_RETURN_STATUS);
    }
    ADC_StopConvert();
    battery_voltage = ADC_CountsTo_Volts((ADC_GetResult16(BATTERY)*2));
    sensor1_voltage = ADC_CountsTo_Volts((ADC_GetResult16(SENSOR1)));
    sensor2_voltage = ADC_CountsTo_Volts((ADC_GetResult16(SENSOR2)));
    sensor3_voltage = ADC_CountsTo_Volts((ADC_GetResult16(SENSOR3)));
    sensor4_voltage = ADC_CountsTo_Volts((ADC_GetResult16(SENSOR4)));
    sensor5_voltage = ADC_CountsTo_Volts((ADC_GetResult16(SENSOR5)));
    sensor6_voltage = ADC_CountsTo_Volts((ADC_GetResult16(SENSOR6)));
    //UpdateTurnData();
}

void UpdateTurnData(void) {
    if (S1_DARK) {
        turn = 1; // left 
    } else if (S3_DARK) {
        turn = 2; // right
    }
}

void AdjustLine(int TurnData)
{ // 0 is adjust after turn, 1 is turn left, 2 is turn right
    if(TurnData == 0) {
        return; //don't want it.
    }
    if (TurnData == 0)
    { // adjust until middle sensor is on line
        //BT_WriteString("TurnData0\n");
        int flag = 0;
        while (flag == 0)
        {
            readADC(); // poll and update adc values - obly thing that may block
            //BT_WriteADC();
            if (S1_DARK)// && S2_LIGHT && S3_LIGHT)
            { // turn left
                //BT_WriteString("TD0-Left\n");
                PivotLeft();
            }
            else if (S3_DARK)// && S2_LIGHT && S1_LIGHT)
            { // turn right
                //BT_WriteString("TD0-Right\n");
                PivotRight();
            }
            else if (S2_DARK) // lowest priority
            {                // line has be adjusted so stop
                CyDelay(15); // to get directly in the middle
                //BT_WriteString("Finished Turning\n");
                Stop(); // stop motor
                LED_OFF;
                flag = 1;
            }
            else
            { // when lost...
                if (turn == 1) // safety net.. not being used
                { // if previously know turn was left then turn left
                    //BT_WriteString("TD0-Lost_TLeft\n");
                    PivotLeft();
                }
                else if (turn == 2)
                { // if previously know turn was right then turn right
                    //BT_WriteString("TD0-Lost_TRight\n");
                    PivotRight();
                }
            }
            CyDelay(25); // enough for s1||s3 to get off line or else it wont aline it will just stay still
        }
        CyDelay(15); // small delay once adjusted for robot momentum to recover
    }
    else if (TurnData == 1)
    { // turn left
        //BT_WriteString("TurnData1\n");
        PivotLeft();
        CyDelay(TURN_DELAY); // enought time for the sensor to come off the line
        readADC();    // refresh adc
        //UpdateTurnData();
        while (!(S2_DARK))
        {
            readADC();
            //UpdateTurnData();
        }
        Stop(); // stop motor
        //LED_ON;
    }
    else if (TurnData == 2)
    { // turn right
        //BT_WriteString("TurnData2\n");
        PivotRight();
        CyDelay(TURN_DELAY); // enough time for the sensor to come off the line
        readADC();    // refresh adc
        //UpdateTurnData();
        while (!(S2_DARK))
        {
            readADC();
            //UpdateTurnData();
        }
        Stop(); // stop motor
        //LED_ON;
    }
}

// Similar to tech test 1: straight line test. BUT with added light sensor functionality (ensure to go straight)
// Depending on values taken from light sensors (90 degree vs slight curve) we adjust motor speed to compensate
void LineTracking(float distance, int food)
{
    int StraightLine = 1;
    deadEnd = 0;
    QuadDec_M2_SetCounter(0);
    QuadDec_M1_SetCounter(0);
    int M2QuadCount = 0;  // init
    float motor2dist = 0; // init
    //int intersection = 0;
    /*char dist[5];
    sprintf(dist, "%.2f", distance); // get value as string
    BT_WriteString("LineTracking...\n");
    BT_WriteString("Target Distance: ");
    BT_WriteString(dist);
    BT_WriteString("\n");*/
    while (StraightLine == 1)
    {
        readADC();
        M2QuadCount = QuadDec_M2_GetCounter();
        motor2dist = ((M2QuadCount / resolution) * cmpulse);
 
        if ((motor2dist > distance) && (food == 1))
        { // food
            BT_WriteString("Reached Food\n");
            Stop();
            StraightLine = 0;
            CyDelay(50);
        }     
        
        if (motor2dist > distance*GRID_SCALE){
            //BT_WriteString("slowdown\n");
            //LED_OFF;
            limit = 66; // slow down
        } else {
            limit = 65;
        }
 
        // simple line tracking
        if (S4_DARK || S6_DARK)
        {
            //Middle sensor starts to go off track and front right sensor has some dark light on it, curve right
            if ((motor2dist > distance * GRID_SCALE) && (food == 0))
            { // reached 85% of distance slow down and look for intersection
                    BT_WriteString("Intersection\n");
                    Stop();
                    limit = 60; // reset limit back
  
                    readADC();
                    BT_WriteADC();
                    //CyDelay(500);
                    StraightLine = 0;
                    //LED_ON;
                    CyDelay(100);
            } else {
                GoStraight(limit);
                CyDelay(1);
            }
        }
        else if (S2_DARK && S4_DARK) {
            GoStraight(limit);
            CyDelay(1);
        }
        else if (S2_DARK && S6_DARK) {
            GoStraight(limit);
            CyDelay(1);
        }
        else if (S2_DARK && S5_DARK)
        {
            //Continue straight
            GoStraight(limit-4);
            CyDelay(1);
        }
        else if (S2_DARK)// && S1_LIGHT && S3_LIGHT)
        {
            GoStraight(limit-2);
            CyDelay(1);
        }
        
        /*else if (S1_DARK && S2_DARK && S3_DARK)
        {
            GoStraight(60);
            CyDelay(1);
        }*/
        else if (S1_DARK && S3_DARK) {
            GoStraight(limit-2);
            CyDelay(50);
        }
        else if (S1_DARK && S2_LIGHT && S3_LIGHT)
        {
            //Curve right
            AdjustLeft(limit-2);
            CyDelay(1);
        }
        else if (S3_DARK && S2_LIGHT && S1_LIGHT)
        {
            //Middle sensor starts to go off track and front left sensor has some dark light on it, curve left
            //Curve left
            AdjustRight(limit-2);
            CyDelay(1);
        }
        else if (S5_DARK && !S2_DARK) {
            GoStraight(limit-2);
            CyDelay(1);
        }

        /*else {
            GoStraight(65);
        }*/
        //if ((int) motor2dist % (int) 12 == 0) { // every 12 cm check for dead ends
        if((DeadEnd()) && (mode == 1)) {
            Stop();
            StraightLine = 0;
        }
            /*if (S1_LIGHT && S2_LIGHT && S3_LIGHT && S4_LIGHT && S5_LIGHT && S6_LIGHT) { // dead end if no sensor is on line
                BT_WriteString("All sensors off\n");
                
                Stop();
                //readADC();
                BT_WriteADC();
                StraightLine = 0;
                deadEnd = 1;
                
                CyDelay(500);
            }*/
        //}


    }
}

//Uses RF to get Dead End
int DeadEnd() {
    int currentPos[2];
    int validFPos, validLPos, validRPos = 0;
    currentPos[0] = getRobotY();
    currentPos[1] = getRobotX();
    
    int frontPos[2], leftPos[2], rightPos[2];
    //updateRFOrientation();
    
    if (mapA[currentPos[0]][currentPos[1]] == 1) {
        return 0;
    }
    
    if (orientation == 1) {
        frontPos[0] = currentPos[0]-1;
        frontPos[1] = currentPos[1];
        leftPos[0] = currentPos[0];
        leftPos[1] = currentPos[1]-1;
        rightPos[0] = currentPos[0];
        rightPos[1] = currentPos[1]+1;
        //Check Left Right and Front
    } else if (orientation == 2) {
        frontPos[0] = currentPos[0];
        frontPos[1] = currentPos[1]+1;
        leftPos[0] = currentPos[0]-1;
        leftPos[1] = currentPos[1];
        rightPos[0] = currentPos[0]+1;
        rightPos[1] = currentPos[1];
        //Check Left Right and Front
    } else if (orientation == 3) {
        //Check Left Right and Front
        frontPos[0] = currentPos[0]+1;
        frontPos[1] = currentPos[1];
        leftPos[0] = currentPos[0];
        leftPos[1] = currentPos[1]+1;
        rightPos[0] = currentPos[0];
        rightPos[1] = currentPos[1]-1;
    } else if (orientation == 4) {
        //Check Left Right and Front
        frontPos[0] = currentPos[0];
        frontPos[1] = currentPos[1]-1;
        leftPos[0] = currentPos[0]+1;
        leftPos[1] = currentPos[1];
        rightPos[0] = currentPos[0]-1;
        rightPos[1] = currentPos[1];
    }
    
    if((frontPos[0] >= 0) && (frontPos[0] < MAXROW) && (frontPos[1] >= 0) && (frontPos[1] < MAXCOL)) {
        validFPos = 1;
    }
    
    if((leftPos[0] >= 0) && (leftPos[0] < MAXROW) && (leftPos[1] >= 0) && (leftPos[1] < MAXCOL)) {
        validLPos = 1;
    }
    
    if((rightPos[0] >= 0) && (rightPos[0] < MAXROW) && (rightPos[1] >= 0) && (rightPos[1] < MAXCOL)) {
        validRPos = 1;
    }
    
    if(validFPos) {
        if(mapA[frontPos[0]][frontPos[1]] == 0) {
            return 0;
        }
        
        if(count == 0) {
            //UART_PutString("DEAD END\n");
            //CyDelay(1000);
            //Stop();
        }   
    }
    
    if (validLPos) {
        if(mapA[leftPos[0]][leftPos[1]] == 0) {
            return 0;
        }
    }
    
    if (validRPos) {
        if(mapA[rightPos[0]][rightPos[1]] == 0) {
            return 0;
        }
    }
    
    //If none of its neighbours are tracks return 1 to signal it is a dead end
    return 1;
    
}

void AStarImplementation(int pathArray[][2], int lengthOfArray, int start[2]) { 
    int i = 0;
    int pathCount = 0;
    int newCol, newRow;
    int currentRow = start[0];
    int currentCol = start[1];
    float distToGo;

    for(i = 1; i < lengthOfArray; i++) {
        newRow = pathArray[i][0];
        newCol = pathArray[i][1];
        if ((orientation == 1) || (orientation == 3)) { //NS
            if((currentCol == newCol) && (i != lengthOfArray-1)) {
                pathCount++;
            } else if ((i == lengthOfArray-1) || (currentCol != newCol)) {
                if((i == lengthOfArray-1) && (currentCol == newCol)) {
                    distToGo = (pathCount+1)*GRID_S;
                } else {
                    distToGo = pathCount*GRID_S;
                }
                
                if((pathCount == 1) && (i != lengthOfArray-1)) {
                    distToGo = 5; //Go the minimum distance
                }
                
                pathCount = 0;
                
                if(newRow > pathArray[i-2][0]) {
                    if(orientation == 1) {
                        UART_PutString("Turn Around\n");
                        TurnAround(); 
                        orientation = 3;
                    }
                } else if (newRow < pathArray[i-2][0]) {
                    if(orientation == 3) {
                        UART_PutString("Turn Around\n");
                        TurnAround();
                        orientation = 1;
                    }
                }
                
                if ((i == lengthOfArray - 1) && (currentCol == newCol)) {
                    UART_PutString("Straight\n");
                    LineTracking(distToGo, 1);
                } else {
                    UART_PutString("Straight\n");
                    LineTracking(distToGo, 0);
                    i--;
                }
                
                if(orientation == 1) {
                    if(newCol < currentCol) {
                        UART_PutString("Turn Left\n");
                        AdjustLine(1);
                        AdjustLine(0);
                        updateOrientation(1);
                    } else if (newCol > currentCol) {
                        UART_PutString("Turn Right\n");
                        AdjustLine(2);
                        AdjustLine(0);
                        updateOrientation(2);
                    }
                } else if (orientation == 3) {
                    if(newCol < currentCol) {
                        UART_PutString("Turn Right\n");
                        AdjustLine(2);
                        AdjustLine(0);
                        updateOrientation(2);
                    } else if (newCol > currentCol) {
                        UART_PutString("Turn Left\n");
                        AdjustLine(1);
                        AdjustLine(0);
                        updateOrientation(1);
                        
                    }
                }
            }
        } else if ((orientation == 2) || (orientation == 4)) { //EW
            if((currentRow == newRow) && (i != lengthOfArray-1)) {
                pathCount++;
            } else if ((i == lengthOfArray-1) || (currentRow != newRow)) {
                if((i == lengthOfArray-1) && (currentRow == newRow)) {
                    distToGo = (pathCount+1)*GRID_L;
                } else {
                    distToGo = pathCount*GRID_L;
                }
                
                if((pathCount == 1) && (i != lengthOfArray-1)) {
                    distToGo = 5;
                }
                
                pathCount = 0;
                
                if(newCol > pathArray[i-2][1]) { //If you need to move right and facing left
                    if(orientation == 4) {
                        UART_PutString("Turn Around\n");
                        TurnAround();
                        orientation = 2;
                    }
                } else if (newCol < pathArray[i-2][1]) { //If you need to move left and facing right
                    if(orientation == 2) {
                        UART_PutString("Turn Around\n");
                        TurnAround();
                        orientation = 4;
                    }
                }
                
                if ((i == lengthOfArray-1) && (currentRow == newRow)) {
                    UART_PutString("Straight\n");
                    LineTracking(distToGo,1);
                } else {
                    UART_PutString("Straight\n");
                    LineTracking(distToGo,0);
                    i--;
                }
                
                if(orientation == 2) {
                    if(newRow > currentRow) {
                        UART_PutString("Turn Right\n");
                        AdjustLine(2);
                        AdjustLine(0);
                        updateOrientation(2);
                    } else if (newRow < currentRow) {
                        UART_PutString("Turn Left\n");
                        AdjustLine(1);
                        AdjustLine(0);
                        updateOrientation(1);
                    }
                } else if (orientation == 4) {
                    if(newRow > currentRow) {
                        UART_PutString("Turn Left\n");
                        AdjustLine(1);
                        AdjustLine(0);
                        updateOrientation(1);
                    } else if (newRow < currentRow) {
                        UART_PutString("Turn Right\n");
                        AdjustLine(2);
                        AdjustLine(0);
                        updateOrientation(2);
                    }
                }
            }
        }
        currentRow = pathArray[i][0];
        currentCol = pathArray[i][1];      
    }
}

void updateRFOrientation() { // get orientation of robot: +- 30 degrees
    //orientation = 0;
    // read rf and set orientation to 1,2,3,4....N,E,S,W dependant on reading
    uint16_t robot_orientation = getRobotOrientation();
    if ( ((0 < robot_orientation) && (robot_orientation < 300)) ||  ((3300 < robot_orientation) && (robot_orientation < 3599))){
        orientation = 2; // E
    } else if ( (2400 < robot_orientation) && (robot_orientation < 3000)){
        orientation = 3; // S
    } else if ( (1500 < robot_orientation) && (robot_orientation < 2100)){
        orientation = 4; // W
    } else if ( (600 < robot_orientation) && (robot_orientation < 1200)){
        orientation = 1; // N
    } else {
        orientation = 0;
    }   
}

void updateOrientation(int turnDir) { // called everytime we turnLeft (= 1) or turnRight (= 2)
    // read rf and set orientation to 1,2,3,4....N,E,S,W
    if (orientation == 0) {
        return;
    }
    if (turnDir == 1) { // left
        if (orientation == 1) {
            orientation = 4;
        } else {
            orientation--;
        }
    } else if (turnDir == 2) {
        if (orientation == 4) {
            orientation = 1;
        } else {
            orientation++;
        }
    }
}

void L(){
    AdjustLine(1);              //l
    AdjustLine(0);
    CyDelay(300);
}

void R(){
    AdjustLine(2);              //R
    AdjustLine(0);
    CyDelay(300);
}

void S(double distance){
    LineTracking(distance*GRID, 0);    //straight
    CyDelay(300);
}

void BT_WriteADC(){
    char value[30]; // used to hold string
    sprintf(value, "Battery Voltage: %.2f V\n", battery_voltage); // get value as string
    UART_PutString(value);
    
    sprintf(value, "Sensor 1 Voltage: %.2f V\n", sensor1_voltage); // get value as string
    UART_PutString(value);
    
    sprintf(value, "Sensor 2 Voltage: %.2f V\n", sensor2_voltage); // get value as string
    UART_PutString(value);
    
    sprintf(value, "Sensor 3 Voltage: %.2f V\n", sensor3_voltage); // get value as string
    UART_PutString(value);
    
    sprintf(value, "Sensor 4 Voltage: %.2f V\n", sensor4_voltage); // get value as string
    UART_PutString(value);
    
    sprintf(value, "Sensor 5 Voltage: %.2f V\n", sensor5_voltage); // get value as string
    UART_PutString(value);
    
    sprintf(value, "Sensor 6 Voltage: %.2f V\n\n", sensor6_voltage); // get value as string
    UART_PutString(value);
}

uint8_t getDirection() {
    return orientation;
}

void setOrientation(int a) {
    orientation = a;
}

void updateThreshold() {
    int currentPos[2];
    currentPos[0] = getRobotY();
    currentPos[1] = getRobotX();
    darkThresh = DARK_LIGHT;
    if ((currentPos[0] >= 9) && (currentPos[0] <= 14)) {
        if((currentPos[1] >= 13) && (currentPos[1] <= 18)) {
        darkThresh = DARK_LIGHT_BAD_CORNER;
        }
    }
}