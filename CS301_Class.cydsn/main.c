/* ========================================
 * Fully working code:
 * PWM      :
 * Encoder  :
 * ADC      :
 * USB      : port displays speed and position.
 * CMD: "PW xx"
 * Copyright Univ of Auckland, 2016
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Univ of Auckland.
 *
 * ========================================
*/
 
// AS OF 3.30PM THIS IS THE WORKING PROJECT!! // dont remove //OKAY
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <project.h>
#include <time.h>
//* ========================================
#include "defines.h"
#include "MotorFunctions.h"
#include "AStar.h"
//#include "data_practice.h"
#include "implementation2017.h"
#include "RF.h"
#include "RobotMovement.h"
//* ========================================
//Main.c DEFINES
#define DIST_SCALE 0.9
//* ========================================
//Main.c GLOBAL VARIABLES
int turningLeft = 0;
int turningRight = 0;
int TL = 0;
int INTL = 0;
int deadend = 0;
//RF VARIABLES
uint8_t numbersReceived = 0;
volatile uint8_t flag_packet = 0;
volatile uint8_t main_rf_done = 0;
volatile char rf_string[RXSTRINGSIZE];
char completeString[RXSTRINGSIZE];
//* ============================================
void mode1();
void mode2(); //AStarPath
//* ============================================
CY_ISR(rxISR)
{
    //Check if RX register is ready
    if (UART_ReadRxStatus() & UART_RX_STS_FIFO_NOTEMPTY)
    {
        rf_string[flag_packet] = UART_ReadRxData();
        if ((flag_packet == 0) && (rf_string[flag_packet] != '#'))
        {
            flag_packet = 0;
            numbersReceived = 0;
        }
        else if (rf_string[flag_packet] == '#')
        {
            flag_packet = 1;
            numbersReceived = 0;
            rf_string[0] = '#';
        }
        else if (rf_string[flag_packet] == ',')
        {
            numbersReceived++;
            flag_packet++;
        }
        else if ((rf_string[flag_packet] == '-') || (rf_string[flag_packet] == '[') || (rf_string[flag_packet] == ']'))
        {
            flag_packet++;
        }
        else if ((rf_string[flag_packet] >= 48) && (rf_string[flag_packet] <= 57))
        {
            flag_packet++;
        }
        else if (rf_string[flag_packet] == '\n')
        {
            numbersReceived++;
            flag_packet = 0;
            strcpy(completeString, (char *)rf_string);
            if (numbersReceived == 17)
            {
                setRFDone();
                main_rf_done = 1;
                parseString(completeString);
                updateThreshold();
                // initOrientation();
            }
        }
    }
}
 
CY_ISR(BT_ISR)
{
    if (UART_ReadRxStatus() & UART_RX_STS_FIFO_NOTEMPTY)
    {
        rf_string[flag_packet] = UART_ReadRxData();
        if (flag_packet == 0)
        {
            if (rf_string[flag_packet] == '&')
            {
                flag_packet++;
            }
            else
            {
                flag_packet = 0;
            }
        }
        else if (rf_string[flag_packet] == '\n')
        {
            flag_packet = 0;
            memset(completeString, 0, 64);
            strcpy(completeString, (char *)rf_string);
            main_rf_done = 1;
        }
        else
        {
            flag_packet++;
        }
    }
}
//* =============================================
int main()
{
    CyDelay(1500);
 
    CyGlobalIntEnable; /* Enable global interrupts. */
 
    UART_Start(); //start uart
    UART_SetRxInterruptMode(UART_RX_STS_FIFO_NOTEMPTY);
 
    isrRF_RX_StartEx(rxISR); //start rx receiving isr
    //isrRF_RX_StartEx(BT_ISR);
 
    LED_ON;
 
    //enable motors
    turningLeft = 0;
    turningRight = 0;
    MotorControlINIT();
    Motor1INIT(127);
    Motor2INIT(127);
 
    QuadDec_M2_Init();
    QuadDec_M1_Init();
 
    QuadDec_M2_Enable();
    QuadDec_M1_Enable();
 
    QuadDec_M2_Start();
    QuadDec_M1_Start();
 
    ADC_Enable();
    ADC_Start();
 
    //TurnLeft(); // stuck here
 
    Stop();
    // Turn left end
 
    RF_BT_SELECT_Write(0);
 
    QuadDec_M1_SetCounter(0);
    QuadDec_M2_SetCounter(0);
    
    setMapData(map);
    
    CyDelay(3000);
    
    if(ModeSelect2_Read() == 1) {
        if(!ModeSelect1_Read()) {
            mode1();
            setMode(1);
        } else {
            mode2();
            setMode(2);
        }
        UART_PutString("Done\n");
    }
    
    //char output[50];
    
    while(1)
    {    
        /*S(0.5);
        L();
        S(0.5);
        R();
        S(0.5);
        R();
        S(0.5);
        R();
        S(0.5);
        R();
        S(0.5);
        L();
        S(0.5);
        L();
        L();*/
        //LineTracking(5,0);
        //CyDelay(3000);
        
        readADC();
        BT_WriteADC();
        //Stop();
        
        /*
        S(1);
        L();
        S(1);
        R();
        S(1);
        R();
        S(1);
        L();
        S(1);
        R();
        S(1);
        */
     
        // SPEED AND DISTANCE REGULATION: M2QA:12.6  M2QB:12.7      M1QA:15.1    M1QB:15.2
        // Goals: regulate motor speed (equal speeds on each motor)
        //    - get to choose how fast the motors run (percentage of max speed possible)
        // Speed relates to PWM; to slow down speed we slow down PWM.
        // with max PWM rating; get speed in rot/s and then slow down as a % of that speed.
 
        /*PWM1duty = PWM_1_ReadCompare(); // Read counter value for PWM1 (M1)
        PWM2duty = PWM_2_ReadCompare(); // Read duty cycle of PWM2 (M2)
        M2QuadCount = QuadDec_M2_GetCounter(); // current value of counter
        M1QuadCount = QuadDec_M1_GetCounter(); // current value of counter - redundant
        motor1dist = ((M1QuadCount/resolution)*cmpulse);
        motor2dist = ((M2QuadCount/resolution)*cmpulse);*/
        //path2();
        //S(6);
        //R();
        //S(2);
        //R();
        //S(1);
        //CyDelay(1000);
        //readADC();
        //BT_WriteADC();
        // start = 0;
        //sprintf(output, "[Row,Col,Orientation]\n[%d,%d,%d]\n",getRobotY(),getRobotX(),getRobotOrientation());
        //UART_PutString(output);
        //CyDelay(1000);
    }
}

//dfs with astar 
void mode1() {
    int start_loc[2];
    start_loc[0] = starty;
    start_loc[1] = startx;
    setOrientation(1); //North
    /*while(main_rf_done == 0);
    start_loc[0] = (int) getRobotY();
    start_loc[1] = (int) getRobotX();
    
    while(map[start_loc[0]][start_loc[1]] == 1) {
        start_loc[0] = (int) getRobotY();
        start_loc[1] = (int) getRobotX();
    }
    */
    while(main_rf_done == 0);
    updateRFOrientation(); //Initialise orientation
    navigateMap(start_loc,map);
    Stop();
}

//AStar to random points on the map until it visits every node
void alt_mode1() {
    int start_loc[2];

    /*while(main_rf_done == 0);
    start_loc[0] = (int) getRobotY();
    start_loc[1] = (int) getRobotX();
    while(map[start_loc[0]][start_loc[1]] == 1) {
        start_loc[0] = (int) getRobotY();
        start_loc[1] = (int) getRobotX();
    }
    */
    start_loc[0] = starty;
    start_loc[1] = startx;
    setOrientation(1); //North
    while(main_rf_done == 0);
    updateRFOrientation(); //Initialise orientation    
    int i,j,visited[MAXROW][MAXCOL];
    int visitCount = 0;
    int target[2];
    for(i = 0; i < MAXROW; i++) {
        for(j = 0; j < MAXCOL; j++) {
            visited[i][j] = map[i][j];
            if(map[i][j] == 1) {
                visitCount++;
            }
        }
    }
    
    while(visitCount != MAXSIZE) {
        srand(time(NULL));
        target[0] = rand() % MAXROW;
        target[1] = rand() % MAXCOL;
        if(visited[target[0]][target[1]] == 0) {
            Node* path = AStar(start_loc,target,map);
            int pathSize = getListSize(path);
            int pathArray[pathSize][2];
            Node* cursor = path;
            for(i = 0; i < pathSize; i++) {
                pathArray[i][0] = cursor->Row;
                pathArray[i][1] = cursor->Col;
                visited[cursor->Row][cursor->Col] = 1;
                visitCount++;
                if(cursor->next != NULL) {
                    cursor = cursor->next;
                }
            }
            deleteList(path);
            free(cursor);
            cursor = NULL;
            AStarImplementation(pathArray,pathSize,start_loc);
            start_loc[0] = target[0];
            start_loc[1] = target[1];
        }
    }
}

void mode2() {

    int start_loc[2];
    //while(main_rf_done == 0);
    //start_loc[0] = (int) getRobotY();
    //start_loc[1] = (int) getRobotX();
    start_loc[0] = starty;
    start_loc[1] = startx;
    
    /*while(map[start_loc[0]][start_loc[1]] == 1) {
        start_loc[0] = (int) getRobotY();
        start_loc[1] = (int) getRobotX();
    }*/
    
    setOrientation(1); // North
    while(main_rf_done == 0);
    updateRFOrientation();

    int numberOfPellets = sizeof(food_list)/sizeof(food_list[0]);
    int i;
    for(i = 0; i < numberOfPellets; i++) {
        Node* path = AStar(start_loc,food_list[i],map);
        int pathSize = getListSize(path);
        int pathArray[pathSize][2];
        Node* cursor = path;
        int j;
        for(j = 0; j < pathSize; j++) {
            pathArray[j][0] = cursor->Row;
            pathArray[j][1] = cursor->Col;
            if(cursor->next != NULL) {
                cursor = cursor->next;
            }
        }
        deleteList(path);
        free(cursor);
        cursor = NULL;
        AStarImplementation(pathArray,pathSize,start_loc);
        start_loc[0] = food_list[i][0];
        start_loc[1] = food_list[i][1];
    }
    Stop();
}
/* [] END OF FILE */