%% This is a shell that you will have to follow strictly. 
% You will use the plotmap() and viewmap() to display the outcome of your algorithm.

% Load sample_data_map_8, three variables will be created in your workspace. These were created as a 
% result of [m,v,s]=dfs('map_8.txt',[14,1],[1,18]);
% The solution can be viewed using 
% plotmap(m,s) 

% write your own function for the DFS algorithm.
function [retmap,retvisited,retsteps] = dfs( mapfile,startlocation,targetlocation)

retmap = map_convert(mapfile);

parentNode = 1; %parentNode
retvisited(parentNode,:) = [startlocation,parentNode]; %[row,col,prev]
currentNode = retvisited(parentNode,:);

while ~( (currentNode(1,1)==targetlocation(1,1)) & (currentNode(1,2)==targetlocation(1,2)) )
    %Check neighbours
    currentRow = currentNode(1,1);
    currentCol = currentNode(1,2);

    %Check Left if not wall AND check if WITHIN BOUNDARIES
    if(currentCol-1 > 0)
        if(retmap(currentRow,currentCol-1) == 0)
            %Check if visited before
            hasVisited = false;
            for i=1:size(retvisited,1)
                if( (currentRow == retvisited(i,1)) & (currentCol-1 == retvisited(i,2)) )
                    hasVisited = true;
                    break
                end
            end
            %If never visited before, add to retVisited & set as new currentNode
            if ~(hasVisited)
                retvisited(size(retvisited,1)+1,:) = [currentRow, currentCol-1, parentNode];
            end
        end
    end

    %Check Right AND if WITHIN BOUNDARIES
    if(currentCol+1 <= size(retmap,2))
        if(retmap(currentRow,currentCol+1) == 0)
            %Check if visited before
            hasVisited = false;
            for i=1:size(retvisited,1)
                if( (currentRow == retvisited(i,1)) & (currentCol+1 == retvisited(i,2)) )
                    hasVisited = true;
                    break
                end
            end
            %If never visited before, add to retVisited & set as new currentNode
            if ~(hasVisited)
                retvisited(size(retvisited,1)+1,:) = [currentRow, currentCol+1, parentNode];
            end
        end
    end


    %Check Up AND if WITHIN BOUNDARIES (currentRow-1 > 0)
    if(currentRow-1 > 0)
        if(retmap(currentRow-1,currentCol) == 0)
            %Check if visited before
            hasVisited = false;
            for i=1:size(retvisited,1)
                if( (currentRow-1 == retvisited(i,1)) & (currentCol == retvisited(i,2)) )
                    hasVisited = true;
                    break
                end
            end
            %If never visited before, add to retVisited & set as new currentNode
            if ~(hasVisited)
                retvisited(size(retvisited,1)+1,:) = [currentRow-1, currentCol, parentNode];
            end
        end
    end
    
    %Check Down AND if WITHIN BOUNDARIES
    if(currentRow+1 <= size(retmap,1))
        if(retmap(currentRow+1,currentCol) == 0)
            %Check if visited before
            hasVisited = false;
            for i=1:size(retvisited,1)
                if( (currentRow+1 == retvisited(i,1)) & (currentCol == retvisited(i,2)) )
                    hasVisited = true;
                    break
                end
            end
            %If never visited before, add to retVisited & set as new currentNode
            if ~(hasVisited)
                retvisited(size(retvisited,1)+1,:) = [currentRow+1, currentCol, parentNode];
            end
        end
    end

    parentNode = parentNode + 1;
    currentNode = retvisited(parentNode,:);
end

i = 1;
while (parentNode ~= 1)
    retsteps(i,:)=[retvisited(parentNode,1),retvisited(parentNode,2)];
    parentNode=retvisited(parentNode,3);
    i = i + 1;
end

%The 2nd node has parentNode 1 so need to add startLocation to the steps.
retsteps(i,:) = startlocation;
retsteps = flipud(retsteps);
end

function placestep(position,i)
% This function will plot a insert yellow rectangle and also print a number in this rectangle. Use with plotmap/viewmap. 
position = [16-position(1) position(2)];
position=[position(2)+0.1 position(1)+0.1];
rectangle('Position',[position,0.8,0.8],'FaceColor','y');
c=sprintf('%d',i);
text(position(1)+0.2,position(2)+0.2,c,'FontSize',10);
end
